package com.bigfans.userservice.api.form;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class UsePropertyForm {

    private String orderId;
    private String couponId;
    private Float points;
    private BigDecimal balance;


}

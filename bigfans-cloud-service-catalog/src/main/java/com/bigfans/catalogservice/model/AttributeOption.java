package com.bigfans.catalogservice.model;

import com.bigfans.catalogservice.model.entity.AttributeOptionEntity;
import lombok.Data;

import java.util.List;

/**
 * 
 * @Title:
 * @Description: 商品属性选项
 * @author lichong
 * @date 2015年9月15日 下午11:30:05
 * @version V1.0
 */
@Data
public class AttributeOption extends AttributeOptionEntity {

	private static final long serialVersionUID = 1387087745406482602L;
	
	private String prodId;
	private String pgId;
	private List<String> values;
	private List<AttributeValue> valueList;

	@Override
	public String toString() {
		return "AttributeOption [prodId=" + prodId + ", pgId=" + pgId + ", values=" + values + ", valueList="
				+ valueList + ", categoryId=" + categoryId + ", name=" + name + ", code=" + code + ", orderNum="
				+ orderNum + ", inputType=" + inputType + ", searchable=" + searchable + ", id=" + id + ", createDate="
				+ createDate + ", updateDate=" + updateDate + ", deleted=" + deleted + "]";
	}

}

package com.bigfans.catalogservice.config;

import com.bigfans.framework.aspect.CacheAspect;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author lichong
 * @create 2018-04-15 上午10:08
 **/
@Configuration
public class CacheConfig {

    @Bean
    public CacheAspect cacheAspect(){
        CacheAspect cacheAspect = new CacheAspect();
        return cacheAspect;
    }

}

package com.bigfans.cartservice;

import com.bigfans.Constants;
import com.bigfans.cartservice.api.auth.CartServiceFunctionalUser;
import com.bigfans.framework.CurrentUser;
import com.bigfans.framework.CurrentUserFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * Hello world!
 *
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableCircuitBreaker
public class CartServiceApp
{
    public static void main( String[] args )
    {
        SpringApplication.run(CartServiceApp.class , args);
    }
}

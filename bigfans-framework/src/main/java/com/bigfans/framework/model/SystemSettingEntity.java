package com.bigfans.framework.model;

import javax.persistence.Table;


/**
 * 
 * @Title: 
 * @Description: 系统配置
 * @author lichong 
 * @date 2015年12月23日 上午10:23:59 
 * @version V1.0
 */
@Table(name="SystemSetting")
public class SystemSettingEntity extends AbstractModel {

	private static final long serialVersionUID = 4283434948157352590L;

	public String getModule() {
		return "SystemSetting";
	}
	
	/** 网站名称 */
	protected String siteName;
	/** 网站网址 */
	protected String portalUrl;
	/** 单点登录地址 */
	protected String ssoLoginUrl;
	/** 单点登出地址 */
	protected String ssoLogoutUrl;
	/** logo */
	protected String logo;
	/** 联系地址 */
	protected String address;
	/** 联系电话 */
	protected String phone;
	/** E-mail */
	protected String email;
	/** 最多保存商品浏览记录条数 */
	protected Integer maxProductsHistorySize;
	/** 商品图片(大)宽度 */
	protected Integer largeProductImageWidth;
	/** 商品图片(大)高度 */
	protected Integer largeProductImageHeight;
	/** 商品图片(中)宽度 */
	protected Integer mediumProductImageWidth;
	/** 商品图片(中)高度 */
	protected Integer mediumProductImageHeight;
	/** 商品缩略图宽度 */
	protected Integer thumbnailProductImageWidth;
	/** 商品缩略图高度 */
	protected Integer thumbnailProductImageHeight;
	/** 默认商品图片(大) */
	protected String defaultLargeProductImage;
	/** 默认商品图片(小) */
	protected String defaultMediumProductImage;
	/** 默认缩略图 */
	protected String defaultThumbnailProductImage;
	/** 本地图片存储路径 */
	protected String localImageDirectory;
	/** 图片服务器 */
	protected String imageStoragePlugin;
	/** 媒体上传路径 */
	protected String mediaServer;
	/** 文件上传路径 */
	protected String fileServer;
	/** SMTP服务器地址 */
	protected String smtpHost;
	/** SMTP服务器端口 */
	protected Integer smtpPort;
	/** SMTP用户名 */
	protected String smtpUsername;
	/** SMTP密码 */
	protected String smtpPassword;
	/** SMTP是否启用SSL */
	protected Boolean smtpSSLEnabled;
	/** 发件人邮箱 */
	protected String smtpFromMail;
	/** 库存警告数 */
	protected Integer stockWarningCount;
	/** Cookie路径 */
	protected String cookiePath;
	/** Cookie作用域 */
	protected String cookieDomain;
	/** 主题 */
	protected String theme;
	/** 七牛云服务器配置 */
	protected String qiniuAccessKey;
	protected String qiniuSecretKey;
	protected String qiniuBucketName;
	protected String qiniuBucketHostName;
	/** 阿里大鱼配置 */
	protected String alidyUrl;
	protected String alidyAppKey;
	protected String alidySecret;
	protected String alidySignName;
	protected String alidy_template_valideCode;
	protected String alidy_template_prodArrival;
	protected String alidy_template_priceDown;

	public String getSiteName() {
		return siteName;
	}

	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}

	public String getPortalUrl() {
		return portalUrl;
	}

	public void setPortalUrl(String portalUrl) {
		this.portalUrl = portalUrl;
	}

	public String getSsoLogoutUrl() {
		return ssoLogoutUrl;
	}

	public void setSsoLogoutUrl(String ssoLogoutUrl) {
		this.ssoLogoutUrl = ssoLogoutUrl;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getLargeProductImageWidth() {
		return largeProductImageWidth;
	}

	public void setLargeProductImageWidth(Integer largeProductImageWidth) {
		this.largeProductImageWidth = largeProductImageWidth;
	}

	public Integer getLargeProductImageHeight() {
		return largeProductImageHeight;
	}

	public void setLargeProductImageHeight(Integer largeProductImageHeight) {
		this.largeProductImageHeight = largeProductImageHeight;
	}

	public Integer getMediumProductImageWidth() {
		return mediumProductImageWidth;
	}

	public void setMediumProductImageWidth(Integer mediumProductImageWidth) {
		this.mediumProductImageWidth = mediumProductImageWidth;
	}

	public Integer getMediumProductImageHeight() {
		return mediumProductImageHeight;
	}

	public void setMediumProductImageHeight(Integer mediumProductImageHeight) {
		this.mediumProductImageHeight = mediumProductImageHeight;
	}

	public Integer getThumbnailProductImageWidth() {
		return thumbnailProductImageWidth;
	}

	public void setThumbnailProductImageWidth(Integer thumbnailProductImageWidth) {
		this.thumbnailProductImageWidth = thumbnailProductImageWidth;
	}

	public Integer getThumbnailProductImageHeight() {
		return thumbnailProductImageHeight;
	}

	public void setThumbnailProductImageHeight(Integer thumbnailProductImageHeight) {
		this.thumbnailProductImageHeight = thumbnailProductImageHeight;
	}

	public String getDefaultLargeProductImage() {
		return defaultLargeProductImage;
	}

	public void setDefaultLargeProductImage(String defaultLargeProductImage) {
		this.defaultLargeProductImage = defaultLargeProductImage;
	}

	public String getDefaultMediumProductImage() {
		return defaultMediumProductImage;
	}

	public void setDefaultMediumProductImage(String defaultMediumProductImage) {
		this.defaultMediumProductImage = defaultMediumProductImage;
	}

	public String getDefaultThumbnailProductImage() {
		return defaultThumbnailProductImage;
	}

	public void setDefaultThumbnailProductImage(String defaultThumbnailProductImage) {
		this.defaultThumbnailProductImage = defaultThumbnailProductImage;
	}

	public String getSmtpHost() {
		return smtpHost;
	}

	public void setSmtpHost(String smtpHost) {
		this.smtpHost = smtpHost;
	}

	public Integer getSmtpPort() {
		return smtpPort;
	}

	public void setSmtpPort(Integer smtpPort) {
		this.smtpPort = smtpPort;
	}

	public String getSmtpUsername() {
		return smtpUsername;
	}

	public void setSmtpUsername(String smtpUsername) {
		this.smtpUsername = smtpUsername;
	}

	public String getSmtpPassword() {
		return smtpPassword;
	}

	public void setSmtpPassword(String smtpPassword) {
		this.smtpPassword = smtpPassword;
	}

	public Boolean getSmtpSSLEnabled() {
		return smtpSSLEnabled;
	}

	public void setSmtpSSLEnabled(Boolean smtpSSLEnabled) {
		this.smtpSSLEnabled = smtpSSLEnabled;
	}

	public String getSmtpFromMail() {
		return smtpFromMail;
	}

	public void setSmtpFromMail(String smtpFromMail) {
		this.smtpFromMail = smtpFromMail;
	}

	public Integer getStockWarningCount() {
		return stockWarningCount;
	}

	public void setStockWarningCount(Integer stockWarningCount) {
		this.stockWarningCount = stockWarningCount;
	}

	public String getCookiePath() {
		return cookiePath;
	}

	public void setCookiePath(String cookiePath) {
		this.cookiePath = cookiePath;
	}

	public String getCookieDomain() {
		return cookieDomain;
	}

	public void setCookieDomain(String cookieDomain) {
		this.cookieDomain = cookieDomain;
	}

	public String getTheme() {
		return theme;
	}

	public void setTheme(String theme) {
		this.theme = theme;
	}

	public String getLocalImageDirectory() {
		return localImageDirectory;
	}

	public void setLocalImageDirectory(String localImageDirectory) {
		this.localImageDirectory = localImageDirectory;
	}

	public String getImageStoragePlugin() {
		return imageStoragePlugin;
	}

	public void setImageStoragePlugin(String imageStoragePlugin) {
		this.imageStoragePlugin = imageStoragePlugin;
	}

	public String getMediaServer() {
		return mediaServer;
	}

	public void setMediaServer(String mediaServer) {
		this.mediaServer = mediaServer;
	}

	public String getFileServer() {
		return fileServer;
	}

	public void setFileServer(String fileServer) {
		this.fileServer = fileServer;
	}

	public String getQiniuAccessKey() {
		return qiniuAccessKey;
	}

	public void setQiniuAccessKey(String qiniuAccessKey) {
		this.qiniuAccessKey = qiniuAccessKey;
	}

	public String getQiniuSecretKey() {
		return qiniuSecretKey;
	}

	public void setQiniuSecretKey(String qiniuSecretKey) {
		this.qiniuSecretKey = qiniuSecretKey;
	}

	public String getQiniuBucketName() {
		return qiniuBucketName;
	}

	public void setQiniuBucketName(String qiniuBucketName) {
		this.qiniuBucketName = qiniuBucketName;
	}

	public String getQiniuBucketHostName() {
		return qiniuBucketHostName;
	}

	public void setQiniuBucketHostName(String qiniuBucketHostName) {
		this.qiniuBucketHostName = qiniuBucketHostName;
	}

	public String getSsoLoginUrl() {
		return ssoLoginUrl;
	}

	public void setSsoLoginUrl(String ssoLoginUrl) {
		this.ssoLoginUrl = ssoLoginUrl;
	}

	public Integer getMaxProductsHistorySize() {
		return maxProductsHistorySize;
	}

	public void setMaxProductsHistorySize(Integer maxProductsHistorySize) {
		this.maxProductsHistorySize = maxProductsHistorySize;
	}

	public String getAlidyUrl() {
		return alidyUrl;
	}

	public void setAlidyUrl(String alidyUrl) {
		this.alidyUrl = alidyUrl;
	}

	public String getAlidyAppKey() {
		return alidyAppKey;
	}

	public void setAlidyAppKey(String alidyAppKey) {
		this.alidyAppKey = alidyAppKey;
	}

	public String getAlidySecret() {
		return alidySecret;
	}

	public void setAlidySecret(String alidySecret) {
		this.alidySecret = alidySecret;
	}

	public String getAlidySignName() {
		return alidySignName;
	}

	public void setAlidySignName(String alidySignName) {
		this.alidySignName = alidySignName;
	}

	public String getAlidy_template_valideCode() {
		return alidy_template_valideCode;
	}

	public void setAlidy_template_valideCode(String alidy_template_valideCode) {
		this.alidy_template_valideCode = alidy_template_valideCode;
	}

	public String getAlidy_template_prodArrival() {
		return alidy_template_prodArrival;
	}

	public void setAlidy_template_prodArrival(String alidy_template_prodArrival) {
		this.alidy_template_prodArrival = alidy_template_prodArrival;
	}

	public String getAlidy_template_priceDown() {
		return alidy_template_priceDown;
	}

	public void setAlidy_template_priceDown(String alidy_template_priceDown) {
		this.alidy_template_priceDown = alidy_template_priceDown;
	}
	
}

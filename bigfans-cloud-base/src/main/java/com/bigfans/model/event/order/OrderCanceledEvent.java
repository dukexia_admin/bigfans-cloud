package com.bigfans.model.event.order;

import lombok.Data;

/**
 * @author lichong
 * @create 2018-02-17 下午2:19
 **/
@Data
public class OrderCanceledEvent {

    private String orderId;

    public OrderCanceledEvent(String orderId) {
        this.orderId = orderId;
    }
}
